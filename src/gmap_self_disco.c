/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_self_disco.h"
#include "gmap_self_bridge.h"
#include "gmap_self_util.h"
#include "gmap_self_eth.h"
#include <amxd/amxd_path.h>
#include <amxs/amxs.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/common_api.h>
#include <gmap/gmap_devices.h>
#include <gmap/gmap_config.h>
#include <malloc.h>
#include <string.h>

#define ME "mod-self"


typedef struct {
    amxc_htable_it_t it;
    /** Never NULL */
    gmap_self_bridge_t* bridge;
    /** query to discover ethernet ports and vaps of `bridge` */
    netmodel_query_t* eth_list_query;
} gmap_self_disco_bridge_t;

struct gmap_self_disco {
    /** query to search which bridge exists */
    netmodel_query_t* bridge_disco_query;
    /**
     * Keys are name of bridge, values are @ref gmap_self_disco_bridge_t.
     */
    amxc_htable_t disco_bridges;
    /**
     * All eth ports (of all bridges and without bridge).
     * Keys are name of ports, values are @ref gmap_self_eth_t.
     */
    amxc_htable_t eths;
    gmap_self_selfdev_t* selfdev;
};

static void s_create_bridge(gmap_self_disco_t* disco, const char* bridge_name);

bool gmap_self_disco_new(gmap_self_disco_t** disco, gmap_self_selfdev_t* selfdev) {
    bool retval = false;
    when_null_trace(disco, error, ERROR, "NULL");
    when_null_trace(selfdev, error, ERROR, "NULL");

    *disco = calloc(1, sizeof(gmap_self_disco_t));
    when_null_trace(*disco, error, ERROR, "Out of mem");

    amxc_htable_init(&(*disco)->disco_bridges, 4);
    amxc_htable_init(&(*disco)->eths, 8);

    (*disco)->selfdev = selfdev;

    retval = true;
    goto cleanup;
error:
    gmap_self_disco_delete(disco);
cleanup:
    return retval;
}

static void s_disco_bridge_delete(gmap_self_disco_bridge_t** disco_bridge, bool also_destroy_in_gmap) {
    if((disco_bridge == NULL) || (*disco_bridge == NULL)) {
        return;
    }
    netmodel_closeQuery((*disco_bridge)->eth_list_query);
    gmap_self_bridge_delete(&(*disco_bridge)->bridge, also_destroy_in_gmap);
    free(*disco_bridge);
    *disco_bridge = NULL;
}

static void s_delete_bridge_and_gmap_device_and_unlink_eths(gmap_self_disco_t* disco, gmap_self_disco_bridge_t* disco_bridge) {
    const char* bridge_name = gmap_self_bridge_name(disco_bridge->bridge);

    amxc_htable_for_each(it, &disco->eths) {
        gmap_self_eth_t* eth = amxc_htable_it_get_data(it, gmap_self_eth_t, it);
        if(gmap_self_eth_bridge(eth) == disco_bridge->bridge) {
            gmap_self_eth_set_bridge(eth, NULL);
        }
    }

    amxc_htable_it_t* it = amxc_htable_take(&disco->disco_bridges, bridge_name);
    amxc_htable_it_clean(it, NULL);
    s_disco_bridge_delete(&disco_bridge, true);
}

/**
 * Implements @amxc_htable_it_delete_t for @gmap_self_disco_bridge_t
 *
 * Does not destroy the bridge device in gmap.
 *
 * Precondition: no ethernet port links to the bridge.
 */
static void s_disco_bridge_delete_it(const char* key UNUSED, amxc_htable_it_t* it) {
    when_null_trace(it, exit, ERROR, "NULL");
    gmap_self_disco_bridge_t* disco_bridge = amxc_htable_it_get_data(it, gmap_self_disco_bridge_t, it);
    s_disco_bridge_delete(&disco_bridge, false);
exit:
    return;
}

void gmap_self_disco_delete(gmap_self_disco_t** disco) {
    if((disco == NULL) || (*disco == NULL)) {
        return;
    }

    netmodel_closeQuery((*disco)->bridge_disco_query);

    amxc_htable_clean(&(*disco)->eths, gmap_self_eth_delete_it);
    amxc_htable_clean(&(*disco)->disco_bridges, s_disco_bridge_delete_it);

    free(*disco);
    *disco = NULL;
}

static void s_delete_disappearing_bridges(gmap_self_disco_t* disco, const amxc_var_t* bridgelist) {
    when_null_trace(disco, exit, ERROR, "NULL");
    when_null_trace(bridgelist, exit, ERROR, "NULL");
    amxc_htable_for_each(it, &disco->disco_bridges) {
        gmap_self_disco_bridge_t* disco_bridge = amxc_htable_it_get_data(it, gmap_self_disco_bridge_t, it);
        const char* bridge_name = gmap_self_bridge_name(disco_bridge->bridge);
        if(!gmap_self_util_varlist_contains_cstring(bridgelist, bridge_name)) {
            s_delete_bridge_and_gmap_device_and_unlink_eths(disco, disco_bridge);
        }
    }
exit:
    return;
}

static void s_add_new_bridges(gmap_self_disco_t* disco, const amxc_var_t* bridgelist) {
    amxc_var_t* bridges_var_of_list_of_csv = gmap_config_get(GMAP_CONFIG_BRIDGES_MODULE_NAME, GMAP_CONFIG_BRIDGES);
    const char* allowed_bridges = GETI_CHAR(bridges_var_of_list_of_csv, 0);
    when_null_trace(allowed_bridges, exit, ERROR, "Not creating bridge(s): Error retrieving config value '" GMAP_CONFIG_BRIDGES "' for module " GMAP_CONFIG_BRIDGES_MODULE_NAME);

    amxc_var_for_each(bridge_var, bridgelist) {
        const char* bridge_name = amxc_var_constcast(cstring_t, bridge_var);
        amxc_htable_it_t* it = amxc_htable_get(&disco->disco_bridges, bridge_name);
        if(!gmap_self_util_csv_contains_cstring(allowed_bridges, bridge_name)) {
            SAH_TRACEZ_INFO(ME, "Not adding bridge %s because not allowed by Devices.Config." GMAP_CONFIG_BRIDGES_MODULE_NAME "." GMAP_CONFIG_BRIDGES, bridge_name);
            continue;
        }
        if(it == NULL) {
            s_create_bridge(disco, bridge_name);
        }
    }
exit:
    amxc_var_delete(&bridges_var_of_list_of_csv);
    return;
}

/**
 * Adds/remove bridges that are new/disappeared in the list `netmodel_bridges`
 *
 * Implements @ref netmodel_callback_t
 */
static void s_on_bridge_list_cbf(const char* sig_name UNUSED, const amxc_var_t* netmodel_bridges, void* priv) {
    gmap_self_disco_t* disco = priv;
    when_null_trace(disco, exit, ERROR, "NULL");

    s_delete_disappearing_bridges(disco, netmodel_bridges);
    s_add_new_bridges(disco, netmodel_bridges);

exit:
    return;
}

/**
 * Adds eth ports of `netmodel_eths` that are not yet in `bridge`.
 *
 * If the eth port exist in another bridge, or is orphaned (has no bridge), moves it to this bridge.
 * Otherwise, create new eth port.
 */
static void s_create_or_move_appearing_eths(gmap_self_disco_t* disco, gmap_self_bridge_t* bridge, const amxc_var_t* netmodel_eths) {
    when_null_trace(disco, exit, ERROR, "NULL");
    when_null_trace(bridge, exit, ERROR, "NULL");
    when_null_trace(netmodel_eths, exit, ERROR, "NULL");

    amxc_var_for_each(bridge_var, netmodel_eths) {
        const char* eth_name = amxc_var_constcast(cstring_t, bridge_var);
        amxc_htable_it_t* it = amxc_htable_get(&disco->eths, eth_name);
        if(it == NULL) {
            gmap_self_eth_t* eth = NULL;
            bool ok = gmap_self_eth_new(&eth, bridge, eth_name);
            when_false_trace(ok, exit, ERROR, "Error creating eth %s", eth_name);
            amxc_htable_insert(&disco->eths, eth_name, &eth->it);
        } else {
            gmap_self_eth_t* eth = amxc_htable_it_get_data(it, gmap_self_eth_t, it);
            gmap_self_bridge_t* eth_old_bridge = gmap_self_eth_bridge(eth);
            if((eth_old_bridge == NULL) || (0 != strcmp(gmap_self_bridge_name(bridge), gmap_self_bridge_name(eth_old_bridge)))) {
                gmap_self_eth_set_bridge(eth, bridge);
            }
        }
    }
exit:
    return;
}

/**
 * Makes sure eth ports `netmodel_eths` not in `bridge` anymore are unlinked from `bridge`
 *
 * @param netmodel_eths *all* ports (ethernet + wifi) under `bridge`.
 *   Ports not in this list are unlinked from the bridge.
 */
static void s_unlink_disappearing_eths(gmap_self_disco_t* disco, gmap_self_bridge_t* bridge, const amxc_var_t* netmodel_eths) {
    when_null_trace(disco, exit, ERROR, "NULL");
    when_null_trace(bridge, exit, ERROR, "NULL");
    when_null_trace(netmodel_eths, exit, ERROR, "NULL");

    amxc_htable_for_each(it, &disco->eths) {
        gmap_self_eth_t* eth = amxc_htable_it_get_data(it, gmap_self_eth_t, it);
        if((gmap_self_eth_bridge(eth) == bridge) &&
           !gmap_self_util_varlist_contains_cstring(netmodel_eths, gmap_self_eth_name(eth))) {
            gmap_self_eth_set_bridge(eth, NULL);
        }
    }
exit:
    return;
}

/**
 * Adds/unlink eth ports that are new/disappeared in the list `netmodel_eths`
 *
 * Implements @ref netmodel_callback_t
 */
static void s_on_eth_list_cbf(const char* query_path, const amxc_var_t* netmodel_eths, void* priv) {
    gmap_self_disco_t* disco = priv;
    amxc_htable_it_t* it;
    gmap_self_bridge_t* bridge = NULL;
    gmap_self_disco_bridge_t* disco_bridge = NULL;
    char* bridge_name = NULL;
    when_null_trace(disco, exit, ERROR, "NULL");
    when_null_trace(query_path, exit, ERROR, "NULL");
    when_null_trace(netmodel_eths, exit, ERROR, "NULL");

    bridge_name = gmap_self_util_netmodel_path_to_intf_name(query_path);
    when_null_trace(bridge_name, exit, ERROR, "Cannot extract intf name from query path %s", query_path);

    it = amxc_htable_get(&disco->disco_bridges, bridge_name);
    when_null_trace(it, exit, ERROR, "Query for non-existing bridge %s", bridge_name);
    disco_bridge = amxc_htable_it_get_data(it, gmap_self_disco_bridge_t, it);
    when_null_trace(disco_bridge, exit, ERROR, "NULL");
    bridge = disco_bridge->bridge;
    when_null_trace(bridge, exit, ERROR, "NULL");

    s_create_or_move_appearing_eths(disco, bridge, netmodel_eths);
    s_unlink_disappearing_eths(disco, bridge, netmodel_eths);

exit:
    free(bridge_name);
    return;
}

static void s_create_bridge(gmap_self_disco_t* disco, const char* bridge_name) {
    bool ok = false;
    gmap_self_disco_bridge_t* disco_bridge = NULL;

    disco_bridge = calloc(1, sizeof(gmap_self_disco_bridge_t));
    when_null_trace(disco_bridge, error, ERROR, "Out of mem");
    ok = gmap_self_bridge_new(&disco_bridge->bridge, disco->selfdev, bridge_name);
    when_false_trace(ok, error, ERROR, "Error creating bridge %s", bridge_name);
    amxc_htable_insert(&disco->disco_bridges, bridge_name, &disco_bridge->it);

    // Note: calls callback immediately, so set up all data structures above.
    disco_bridge->eth_list_query = netmodel_openQuery_getIntfs(bridge_name, "gmap-self", "eth_intf || ssid || wds || moca", netmodel_traverse_down, s_on_eth_list_cbf, disco);
    when_null_trace(disco_bridge->eth_list_query, error, ERROR, "Error creating query");


    return;
error:
    if(disco_bridge != NULL) {
        netmodel_closeQuery(disco_bridge->eth_list_query);

        amxc_htable_it_take(&disco_bridge->it);
        amxc_htable_it_clean(&disco_bridge->it, NULL);
        gmap_self_bridge_delete(&disco_bridge->bridge, false);
        free(disco_bridge);
    }
}


bool gmap_self_disco_start(gmap_self_disco_t* disco) {
    bool retval = false;
    when_null_trace(disco, exit, ERROR, "NULL");

    // Note: "resolver" is always available in Netmodel ("ip-loopback" is not immediately available
    //       during startup). This is important because running a query on an interface that is not
    //       available yet, fails.
    disco->bridge_disco_query = netmodel_openQuery_getIntfs("resolver", "gmap-self", "bridge", netmodel_traverse_all, s_on_bridge_list_cbf, disco);
    when_null_trace(disco->bridge_disco_query, exit, ERROR, "Error creating query");

    // Trace-log to be able to diagnose very rare case of query not opening if it happens again.
    SAH_TRACEZ_WARNING(ME, "Successfully opened netmodel query for receiving list of bridges");

    retval = true;
exit:
    return retval;
}

