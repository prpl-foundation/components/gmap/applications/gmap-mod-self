/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_self.h"
#include "gmap_self_disco.h"
#include "gmap_self_selfdev.h"
#include "gmap_self_util.h"
#include <amxs/amxs.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <malloc.h>
#include <string.h>

#define ME "mod-self"

/**
 * @file Creates all local devices (self-device, bridges, and ethernet ports).
 */

typedef struct gmap_self {
    gmap_self_disco_t* disco;
    gmap_self_selfdev_t* selfdev;
} gmap_self_t;


/**
 * Creates all local devices (the self-device, the bridges, and the ethernet ports).
 *
 * Cannot be called twice.
 */
static bool s_start(gmap_self_t* self, const char* selfdev_name, gmap_self_selfdev_devtype_e devtype) {
    bool retval = false;
    bool ok = false;
    when_null_trace(self, error, ERROR, "NULL");
    when_str_empty_trace(selfdev_name, error, ERROR, "EMPTY");
    when_false_trace(self->selfdev == NULL && self->disco == NULL, error, ERROR, "Cannot be called twice");

    // create selfdev
    ok = gmap_self_selfdev_new(&self->selfdev, selfdev_name, devtype);
    when_false_trace(ok, error, ERROR, "Error creating selfdev");

    // create bridges and their children (eth infs)
    ok = gmap_self_disco_new(&self->disco, self->selfdev);
    when_false_trace(ok, error, ERROR, "Failed creating bridge discovery");
    ok = gmap_self_disco_start(self->disco);
    when_false_trace(ok, error, ERROR, "Failed starting bridge discovery");

    retval = true;
error:
    return retval;
}

bool gmap_self_new(gmap_self_t** self, const char* selfdev_name, gmap_self_selfdev_devtype_e devtype) {
    bool ok = false;
    when_null_trace(self, error, ERROR, "NULL");

    *self = calloc(1, sizeof(gmap_self_t));
    when_null_trace(*self, error, ERROR, "Out of mem");

    ok = s_start(*self, selfdev_name, devtype);
    when_false_trace(ok, error, ERROR, "Error starting");

    return true;
error:
    return false;
}


void gmap_self_delete(gmap_self_t** self) {
    if((self == NULL) || (*self == NULL)) {
        return;
    }
    gmap_self_disco_delete(&(*self)->disco);
    gmap_self_selfdev_delete(&(*self)->selfdev);
    free(*self);
    *self = NULL;
}
