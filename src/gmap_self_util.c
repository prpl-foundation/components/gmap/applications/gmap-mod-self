/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_self_util.h"
#include "gmap_self_entrypoint.h"
#include <netmodel/common_api.h>
#include <netmodel/client.h>
#include <gmap/gmap.h>
#include <gmap/gmap_device.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <amxs/amxs_util.h>
#include <amxd/amxd_path.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define ME "mod-self"
#define ACTIVE_SOURCE_FROM_NETMODEL "mod-self-from-netmodel"
#define ACTIVE_PRIORITY 100

/**
 * Converts a path like "NetModel.Intf.lan." to "NetModel.Intf.4.".
 *
 * Precondition: netmodel bus context is the correct bus context for this path.
 */
static amxc_string_t* s_named_netmodel_path_to_index_path(const char* source_path) {
    amxc_string_t* compatible_path = NULL;
    amxc_var_t data;
    amxb_bus_ctx_t* ctx = netmodel_get_amxb_bus();
    int status = -1;
    amxc_var_t* item = NULL;
    const char* key = NULL;
    amxc_var_init(&data);
    when_str_empty_trace(source_path, cleanup, ERROR, "EMPTY");
    when_null_trace(ctx, cleanup, ERROR, "NetModel not initialized");

    status = amxb_get(ctx, source_path, 0, &data, 5);
    when_failed_trace(status, cleanup, ERROR, "Error getting '%s': %d", source_path, status);
    item = amxc_var_get_first(amxc_var_get_first(&data));
    when_null_trace(item, cleanup, ERROR, "Empty return when getting '%s'", source_path);

    key = amxc_var_key(item);
    when_null_trace(key, cleanup, ERROR, "No key for '%s'", source_path);
    amxc_string_new(&compatible_path, 32);
    amxc_string_set(compatible_path, key);

cleanup:
    amxc_var_clean(&data);
    return compatible_path;
}

char* gmap_self_util_get_netmodel_rel_path(const char* netmodel_name) {
    amxc_string_t* path = NULL;
    amxc_string_t abs_path_incompatible;
    char* path_cstring = NULL;
    amxc_string_init(&abs_path_incompatible, 32);
    when_str_empty_trace(netmodel_name, leave, ERROR, "EMPTY");

    // e.g. "NetModel.Intf.eth0."
    amxc_string_setf(&abs_path_incompatible, "%s.Intf.%s.", netmodel_get_root_obj_name(), netmodel_name);

    // e.g. "NetModel.Intf.7."
    path = s_named_netmodel_path_to_index_path(amxc_string_get(&abs_path_incompatible, 0));

    // e.g. "Intf.7."
    amxc_string_remove_at(path, 0, strlen(netmodel_get_root_obj_name()) + 1);

    path_cstring = amxc_string_take_buffer(path);

leave:
    amxc_string_clean(&abs_path_incompatible);
    free(path);
    return path_cstring;
}

char* gmap_self_util_get_gmap_rel_path(const char* name_gmap) {
    amxc_string_t rel_path_gmap;
    char* rel_path_return = NULL;

    when_str_empty_trace(name_gmap, error, ERROR, "EMPTY");

    amxc_string_init(&rel_path_gmap, 0);
    amxc_string_setf(&rel_path_gmap, "Device.%s.", name_gmap);
    rel_path_return = amxc_string_take_buffer(&rel_path_gmap);
    amxc_string_clean(&rel_path_gmap);

    return rel_path_return;

error:
    return NULL;
}

char* gmap_self_util_get_gmap_abs_path(const char* name_gmap) {
    amxc_string_t abs_path_gmap;
    char* abs_path_return = NULL;

    when_str_empty_trace(name_gmap, error, ERROR, "EMPTY");

    amxc_string_init(&abs_path_gmap, 0);
    amxc_string_setf(&abs_path_gmap, "Devices.Device.%s.", name_gmap);
    abs_path_return = amxc_string_take_buffer(&abs_path_gmap);
    amxc_string_clean(&abs_path_gmap);

    return abs_path_return;

error:
    return NULL;
}

/** Convert `Devices.Device.mydev.` into `mydev` */
static char* s_name_from_gmap_path(const char* path_string) {
    amxd_path_t path;
    size_t len = 0;
    char* name = NULL;

    amxd_path_init(&path, path_string);
    name = amxd_path_get_last(&path, false);
    amxd_path_clean(&path);

    // Remove trailing dot (i.e. "mydev." -> "mydev")
    len = strlen(name);
    if(len > 0) {
        name[len - 1] = '\0';
    }

    return name;
}

amxs_status_t gmap_self_util_active_sync_action_cb(UNUSED const amxs_sync_entry_t* entry,
                                                   UNUSED amxs_sync_direction_t direction,
                                                   amxc_var_t* data,
                                                   UNUSED void* priv) {

    bool ok = false;
    const char* path = GET_CHAR(data, "path");
    bool active = GETP_BOOL(data, "parameters.Active");
    char* name = s_name_from_gmap_path(path);

    ok = gmap_device_setActive(name, active, ACTIVE_SOURCE_FROM_NETMODEL, ACTIVE_PRIORITY);
    if(!ok) {
        SAH_TRACEZ_ERROR(ME, "Error setting Active of '%s' to %d", name, active);
    }

    free(name);
    return amxs_status_ok;
}

amxs_status_t gmap_self_util_set_action_cb(UNUSED const amxs_sync_entry_t* entry,
                                           UNUSED amxs_sync_direction_t direction,
                                           amxc_var_t* data,
                                           UNUSED void* priv) {
    bool ok = false;
    const char* path = GET_CHAR(data, "path");
    char* name = s_name_from_gmap_path(path);
    amxc_var_t* parameters = GET_ARG(data, "parameters");

    ok = gmap_device_set(name, parameters);
    if(!ok) {
        SAH_TRACEZ_ERROR(ME, "Error setting parameters of '%s'", name);
    }

    free(name);
    return ok ? amxs_status_ok : amxs_status_unknown_error;
}

amxs_sync_object_t* gmap_self_util_create_sync_obj(amxs_sync_ctx_t* sync_ctx,
                                                   const char* name_netmodel, const char* name_gmap) {
    char* rel_path_gmap = NULL;
    char* rel_path_netmodel = NULL;
    amxs_status_t status = amxs_status_unknown_error;
    amxs_sync_object_t* sync_obj = NULL;
    when_str_empty_trace(name_netmodel, exit, ERROR, "Invalid argument");
    when_str_empty_trace(name_gmap, exit, ERROR, "Invalid argument");

    rel_path_netmodel = gmap_self_util_get_netmodel_rel_path(name_netmodel);
    when_null_trace(rel_path_netmodel, exit, ERROR, "Error getting rel path for %s", name_netmodel);
    rel_path_gmap = gmap_self_util_get_gmap_rel_path(name_gmap);
    when_null_trace(rel_path_gmap, exit, ERROR, "Error getting rel path for %s", name_gmap);

    status = amxs_sync_object_new(&sync_obj, rel_path_netmodel, rel_path_gmap, AMXS_SYNC_ONLY_A_TO_B, NULL, NULL, NULL);
    when_failed_trace(status, exit, ERROR, "Error creating sync object for %s->%s", name_netmodel, name_gmap);
    status = amxs_sync_ctx_add_object(sync_ctx, sync_obj);
    when_failed_trace(status, exit, ERROR, "Error ading sync object for %s->%s", name_netmodel, name_gmap);

exit:
    free(rel_path_gmap);
    free(rel_path_netmodel);
    return sync_obj;
}

char* gmap_self_util_get_netmodel_root_path(void) {
    amxc_string_t netmodel_path;
    amxc_string_init(&netmodel_path, 16);
    amxc_string_setf(&netmodel_path, "%s.", netmodel_get_root_obj_name());
    char* path = amxc_string_take_buffer(&netmodel_path);
    amxc_string_clean(&netmodel_path);
    return path;
}

/**
 * Called when netmodel knows and changes the mac address of an interfaces. Sets it in gmap.
 *
 * Implements @ref netmodel_callback_t
 */
static void s_netmodel_mac_param_cb(const char* sig_name UNUSED, const amxc_var_t* data, void* priv) {
    const char* intf_name = priv;
    bool ok = false;
    amxc_var_t parameters;
    amxc_var_init(&parameters);
    const char* mac_address = amxc_var_constcast(cstring_t, amxc_var_get_first(data));

    // Can happen when ethernet port becomes bridgeless.
    if(mac_address == NULL) {
        mac_address = "";
    }

    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &parameters, "PhysAddress", mac_address);

    ok = gmap_device_set(intf_name, &parameters);
    when_false_trace(ok, exit, ERROR, "Error setting mac addr %s %s", intf_name, mac_address);

exit:
    amxc_var_clean(&parameters);
}

netmodel_query_t* gmap_self_util_start_mac_sync(const char* intf_name) {
    netmodel_query_t* query = NULL;
    when_str_empty_trace(intf_name, exit, ERROR, "Invalid argument");

    query = netmodel_openQuery_getParameters(intf_name, "gmap-self", "MACAddress", NULL,
                                             netmodel_traverse_up, s_netmodel_mac_param_cb, (void*) intf_name);
    when_null_trace(query, exit, ERROR, "%s: cannot open query", intf_name);

exit:
    return query;
}

bool gmap_self_util_sync_ctx_add(amxs_sync_ctx_t* sync_ctx, const char* param_from,
                                 const char* param_to) {
    param_to = param_to != NULL ? param_to : param_from;
    amxs_status_t status = amxs_sync_ctx_add_new_param(sync_ctx, param_from,
                                                       param_to,
                                                       0,
                                                       amxs_sync_param_copy_trans_cb,
                                                       gmap_self_util_set_action_cb,
                                                       NULL);
    when_failed_trace(status, exit, ERROR, "Cannot add sync param %s->%s: %d", param_from, param_to, status);
exit:
    return status == amxs_status_ok;
}

bool gmap_self_util_sync_obj_add(amxs_sync_object_t* sync_obj, const char* param_from,
                                 const char* param_to) {
    param_to = param_to != NULL ? param_to : param_from;
    amxs_status_t status = amxs_sync_object_add_new_param(sync_obj,
                                                          param_from,
                                                          param_to,
                                                          0,
                                                          amxs_sync_param_copy_trans_cb,
                                                          gmap_self_util_set_action_cb,
                                                          NULL);
    when_failed_trace(status, exit, ERROR, "Cannot add sync param %s->%s: %d", param_from, param_to, status);
exit:
    return status == amxs_status_ok;
}

bool gmap_self_util_varlist_contains_cstring(const amxc_var_t* list, const char* needle) {
    when_null_trace(list, exit, ERROR, "NULL");
    when_null_trace(needle, exit, ERROR, "NULL");

    amxc_var_for_each(it, list) {
        const char* candidate = amxc_var_constcast(cstring_t, it);
        if((candidate != NULL) && (0 == strcmp(needle, candidate))) {
            return true;
        }
    }
exit:
    return false;
}

bool gmap_self_util_csv_contains_cstring(const char* list_csv, const char* needle) {
    bool found = false;
    amxc_string_t list_csv_amxstr;
    amxc_var_t list_var;
    amxc_string_split_status_t status = AMXC_ERROR_STRING_SPLIT_INVALID_INPUT;
    amxc_string_init(&list_csv_amxstr, 0);
    amxc_var_init(&list_var);
    when_null_trace(list_csv, exit, ERROR, "NULL");
    when_null_trace(needle, exit, ERROR, "NULL");

    amxc_string_set(&list_csv_amxstr, list_csv);
    status = amxc_string_csv_to_var(&list_csv_amxstr, &list_var, NULL);
    when_failed_trace(status, exit, ERROR, "Failed to split csv %s", list_csv);

    found = gmap_self_util_varlist_contains_cstring(&list_var, needle);
exit:
    amxc_var_clean(&list_var);
    amxc_string_clean(&list_csv_amxstr);
    return found;
}

char* gmap_self_util_netmodel_path_to_intf_name(const char* path) {
    int dot_pos = 0;
    char* intf_name = NULL;
    amxc_string_t string;
    amxc_string_init(&string, 0);
    when_null_trace(path, exit, ERROR, "NULL");

    // length of "NetModel.Intf."
    size_t netmodel_intf_length = strlen(netmodel_get_root_obj_name()) + 6;

    // e.g. "NetModel.Intf.bridge-lan_bridge.Query.77."
    amxc_string_set(&string, path);

    // e.g. "bridge-lan_bridge.Query.77."
    when_false_trace(strlen(path) > netmodel_intf_length, exit, ERROR, "Too short");
    amxc_string_remove_at(&string, 0, netmodel_intf_length);

    // e.g. "bridge-lan_bridge"
    dot_pos = amxc_string_search(&string, ".", 0);
    intf_name = amxc_string_take_buffer(&string);
    // if no terminating dot (e.g. starting from "NetModel.Intf.bridge-lan_bridge"), stop here
    when_true(dot_pos == -1, exit);
    when_str_empty_trace(intf_name, exit, ERROR, "Error extracting intf from path %s", path);
    intf_name[dot_pos] = '\0';

exit:
    amxc_string_clean(&string);
    return intf_name;
}
