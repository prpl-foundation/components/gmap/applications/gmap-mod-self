/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_selfdev.h"
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "gmap_self_selfdev.h"
#include "../common/mock_gmap.h"

#include <debug/sahtrace.h>
#include "../common/test-setup.h"
#include <amxd/amxd_object_parameter.h>
#include <amxc/amxc_macros.h>
#include <amxs/amxs.h>

void test_gmap_self_selfdev_create__normal_case(UNUSED void** state) {
    bool ok = false;
    gmap_self_selfdev_t* selfdev = NULL;

    // GIVEN populated deviceinfo
    test_setup_parse_odl("../common/data/deviceinfo.odl");

    // EXPECT gmap device creation will happen with correct key, disco-source and tags
    mock_gmap_expect_createDevice("my_selfdev_name", "gmap-self", "self protected physical hgw", true, NULL, true);

    // EXPECT device will be set as active
    mock_gmap_expect_setActive("my_selfdev_name", true, true);

    // WHEN creating a selfdev
    ok = gmap_self_selfdev_new(&selfdev, "my_selfdev_name", GMAP_SELF_SELFDEV_DEVTYPE_HGW);
    handle_events();

    // THEN it synced the fields into gmap's HGW device
    assert_true(ok);
    mock_gmap_assert_device_param_string("my_selfdev_name", "SoftwareVersion", "SNAPSHOT v0.3");
    mock_gmap_assert_device_param_string("my_selfdev_name", "Description", "Fancy System SNAPSHOT r0-123456abcdef");
    mock_gmap_assert_device_param_string("my_selfdev_name", "ModelName", "Manu Model B rev 3.1");
    mock_gmap_assert_device_param_string("my_selfdev_name", "Manufacturer", "Good Manufacturing Inc.");
    mock_gmap_assert_device_param_string("my_selfdev_name", "SerialNumber", "SN001234567899");
    mock_gmap_assert_device_param_string("my_selfdev_name", "HardwareVersion", "goodmanu/gm12345");
    mock_gmap_assert_device_param_string("my_selfdev_name", "ProductClass", "goodm-v123");

    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_selfdev_create__repeater(UNUSED void** state) {
    bool ok = false;
    gmap_self_selfdev_t* selfdev = NULL;

    // GIVEN populated deviceinfo
    test_setup_parse_odl("../common/data/deviceinfo.odl");

    // EXPECT gmap device creation will happen without "hgw" tag
    mock_gmap_expect_createDevice("selfbox", "gmap-self", "self protected physical", true, NULL, true);

    // EXPECT device will be set as active
    mock_gmap_expect_setActive("selfbox", true, true);

    // WHEN creating a selfdev with wifirepeater type
    ok = gmap_self_selfdev_new(&selfdev, "selfbox", GMAP_SELF_SELFDEV_DEVTYPE_WIFIREPEATER);
    handle_events();

    // THEN it synced the fields into gmap's wifirepeater device
    assert_true(ok);
    mock_gmap_assert_device_param_string("selfbox", "SoftwareVersion", "SNAPSHOT v0.3");
    mock_gmap_assert_device_param_string("selfbox", "Description", "Fancy System SNAPSHOT r0-123456abcdef");
    mock_gmap_assert_device_param_string("selfbox", "ModelName", "Manu Model B rev 3.1");
    mock_gmap_assert_device_param_string("selfbox", "Manufacturer", "Good Manufacturing Inc.");
    mock_gmap_assert_device_param_string("selfbox", "SerialNumber", "SN001234567899");
    mock_gmap_assert_device_param_string("selfbox", "HardwareVersion", "goodmanu/gm12345");
    mock_gmap_assert_device_param_string("selfbox", "ProductClass", "goodm-v123");

    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_selfdev_create__fail_device_creation(UNUSED void** state) {
    gmap_self_selfdev_t* selfdev = NULL;
    bool ok = false;

    // GIVEN populated deviceinfo
    test_setup_parse_odl("../common/data/deviceinfo.odl");

    // EXPECT gmap device creation will fail
    mock_gmap_expect_createDevice("my_selfdev_name", "gmap-self", "self protected physical hgw", true, NULL, false);

    // WHEN creating a selfdev
    ok = gmap_self_selfdev_new(&selfdev, "my_selfdev_name", GMAP_SELF_SELFDEV_DEVTYPE_HGW);

    // THEN it reported to be failed
    assert_false(ok);
    assert_null(selfdev);
}
