/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock_netmodel.h"
#include "test-setup.h"
#include <amxc/amxc_macros.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <debug/sahtrace.h>
#include "dummy.h"


amxb_bus_ctx_t* __wrap_netmodel_get_amxb_bus(void) {
    return test_setup_bus_ctx();
}

bool __wrap_netmodel_initialize(void) {
    return (bool) mock();
}

amxc_var_t* __wrap_netmodel_getParameters(const char* const interface,
                                          const char* const name,
                                          const char* const flag,
                                          const char* const traverse) {

    amxc_var_t* rv = (amxc_var_t*) mock();
    check_expected(interface);
    check_expected(name);
    check_expected(flag);
    check_expected(traverse);

    return rv;
}

void mock_netmodel_expect_getParameters(const char* const expect_interface,
                                        const char* const expect_name,
                                        const char* const expect_flag,
                                        const char* const expect_traverse,
                                        const char* const return_value) {

    expect_string(__wrap_netmodel_getParameters, interface, expect_interface);
    expect_string(__wrap_netmodel_getParameters, name, expect_name);
    expect_value(__wrap_netmodel_getParameters, flag, expect_flag);
    expect_string(__wrap_netmodel_getParameters, traverse, expect_traverse);

    amxc_var_t* retval_var;
    amxc_var_new(&retval_var);
    amxc_var_set_type(retval_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, retval_var, expect_interface, return_value);
    will_return(__wrap_netmodel_getParameters, retval_var);
}

char* __wrap_netmodel_luckyIntf(const char* const interface,
                                const char* const flag,
                                const char* const traverse) {
    char* rv = (char*) mock();
    check_expected(interface);
    check_expected(flag);
    check_expected(traverse);

    return rv;
}

void mock_netmodel_expect_luckyIntf(const char* const expect_interface,
                                    const char* const expect_flag,
                                    const char* const expect_traverse,
                                    const char* const return_value) {

    expect_string(__wrap_netmodel_luckyIntf, interface, expect_interface);
    expect_string(__wrap_netmodel_luckyIntf, flag, expect_flag);
    expect_string(__wrap_netmodel_luckyIntf, traverse, expect_traverse);

    will_return(__wrap_netmodel_luckyIntf, strdup(return_value));
}

amxc_var_t* __wrap_netmodel_getIntfs(const char* const interface UNUSED,
                                     const char* const flag UNUSED,
                                     const char* const traverse UNUSED) {

    fail_msg("do not use getIntfs because the interfaces are not available close to startup");
    return NULL;
}


netmodel_query_t* __wrap_netmodel_openQuery_getIntfs(const char* intf,
                                                     const char* subscriber,
                                                     const char* flag,
                                                     const char* traverse,
                                                     netmodel_callback_t handler,
                                                     void* userdata) {

    mock_netmodel_query_t* rv = (mock_netmodel_query_t*) mock();
    check_expected(intf);
    check_expected(subscriber);
    check_expected(flag);
    check_expected(traverse);
    assert_non_null(handler);
    if(rv != NULL) {
        rv->callback = handler;
        rv->userdata = userdata;
        rv->intfname = strdup(intf);
        mock_netmodel_call_getintf_callback(rv, rv->intf1, rv->intf2, rv->intf2);
    }

    return (netmodel_query_t*) rv;
}

void mock_netmodel_call_getAddrs_callback(mock_netmodel_query_t* query, const char* data_json_file) {
    amxc_var_t* data = dummy_read_json_from_file(data_json_file);
    assert_non_null(data);
    query->callback(NULL, data, query->userdata);
    amxc_var_delete(&data);
}

netmodel_query_t* __wrap_netmodel_openQuery_getAddrs(const char* intf,
                                                     const char* subscriber,
                                                     const char* flag,
                                                     const char* traverse,
                                                     netmodel_callback_t handler,
                                                     void* userdata) {

    mock_netmodel_query_t* rv = (mock_netmodel_query_t*) mock();
    check_expected(intf);
    check_expected(subscriber);
    check_expected(flag);
    check_expected(traverse);
    assert_non_null(handler);
    if(rv != NULL) {
        rv->callback = handler;
        rv->userdata = userdata;
        rv->intfname = strdup(intf);
        mock_netmodel_call_getAddrs_callback(rv, rv->init_cb_data_json);
    }

    return (netmodel_query_t*) rv;
}

void mock_netmodel_expect_openQuery_getAddrs(const char* expect_intf,
                                             const char* expect_flag,
                                             const char* expect_traverse,
                                             mock_netmodel_query_t* returnvalue) {

    expect_string(__wrap_netmodel_openQuery_getAddrs, intf, expect_intf);
    expect_string(__wrap_netmodel_openQuery_getAddrs, subscriber, "gmap-self");
    if(expect_flag != NULL) {
        expect_string(__wrap_netmodel_openQuery_getAddrs, flag, expect_flag);
    } else {
        expect_value(__wrap_netmodel_openQuery_getAddrs, flag, expect_flag);
    }
    expect_string(__wrap_netmodel_openQuery_getAddrs, traverse, expect_traverse);
    will_return(__wrap_netmodel_openQuery_getAddrs, returnvalue);
}

void __wrap_netmodel_closeQuery(netmodel_query_t* netmodel_query) {
    mock_netmodel_query_t* mock_query = (mock_netmodel_query_t*) netmodel_query;
    if(netmodel_query == NULL) {
        return;
    }
    free(mock_query->intfname);
    mock_query->intfname = NULL;
}

void mock_netmodel_expect_openQuery_getIntfs(const char* expect_intf,
                                             const char* expect_flag,
                                             const char* expect_traverse,
                                             mock_netmodel_query_t* returnvalue) {

    expect_string(__wrap_netmodel_openQuery_getIntfs, intf, expect_intf);
    expect_string(__wrap_netmodel_openQuery_getIntfs, subscriber, "gmap-self");
    expect_string(__wrap_netmodel_openQuery_getIntfs, flag, expect_flag);
    expect_string(__wrap_netmodel_openQuery_getIntfs, traverse, expect_traverse);
    will_return(__wrap_netmodel_openQuery_getIntfs, returnvalue);
}

void mock_netmodel_call_getintf_callback(mock_netmodel_query_t* query, const char* bridge1, const char* bridge2, const char* bridge3) {
    char query_path[64] = {0};
    int query_path_bytes_written = 0;
    amxc_var_t bridges_intfs;
    amxc_var_init(&bridges_intfs);
    amxc_var_set_type(&bridges_intfs, AMXC_VAR_ID_LIST);
    assert_non_null(query->intfname);
    if(bridge1 != NULL) {
        amxc_var_add_new_cstring_t(&bridges_intfs, bridge1);
    }
    if(bridge2 != NULL) {
        amxc_var_add_new_cstring_t(&bridges_intfs, bridge2);
    }
    if(bridge3 != NULL) {
        amxc_var_add_new_cstring_t(&bridges_intfs, bridge3);
    }
    query_path_bytes_written = snprintf(query_path, sizeof(query_path), "NetModel.Intf.%s.Query.123", query->intfname);
    assert_true(query_path_bytes_written < (int) sizeof(query_path));
    query->callback(query_path, &bridges_intfs, query->userdata);
    amxc_var_clean(&bridges_intfs);
    handle_events();
}

netmodel_query_t* __wrap_netmodel_openQuery_getParameters(const char* intf,
                                                          const char* subscriber,
                                                          const char* name,
                                                          const char* flag,
                                                          const char* traverse,
                                                          netmodel_callback_t handler,
                                                          void* userdata) {
    {

        mock_netmodel_query_t* rv = (mock_netmodel_query_t*) mock();
        check_expected(intf);
        check_expected(subscriber);
        check_expected(name);
        check_expected(flag);
        check_expected(traverse);
        assert_non_null(handler);
        if(rv != NULL) {
            rv->callback = handler;
            rv->userdata = userdata;
            rv->intfname = strdup(intf);
            if(rv->key != NULL) {
                mock_netmodel_call_getParameters_callback(rv, rv->key, rv->value);
            }
        }

        return (netmodel_query_t*) rv;
    }
}


void mock_netmodel_expect_openQuery_getParameters(const char* expect_intf,
                                                  const char* expect_name,
                                                  const char* expect_flag,
                                                  const char* expect_traverse,
                                                  mock_netmodel_query_t* returnvalue) {

    expect_string(__wrap_netmodel_openQuery_getParameters, intf, expect_intf);
    expect_string(__wrap_netmodel_openQuery_getParameters, name, expect_name);
    expect_string(__wrap_netmodel_openQuery_getParameters, subscriber, "gmap-self");
    if(expect_flag != NULL) {
        expect_string(__wrap_netmodel_openQuery_getParameters, flag, expect_flag);
    } else {
        expect_value(__wrap_netmodel_openQuery_getParameters, flag, NULL);
    }
    expect_string(__wrap_netmodel_openQuery_getParameters, traverse, expect_traverse);
    will_return(__wrap_netmodel_openQuery_getParameters, returnvalue);
}

void mock_netmodel_call_getParameters_callback(mock_netmodel_query_t* query, const char* key, const char* value) {
    amxc_var_t query_result_data;
    amxc_var_init(&query_result_data);
    amxc_var_set_type(&query_result_data, AMXC_VAR_ID_HTABLE);
    assert_non_null(query->intfname);
    amxc_var_add_new_key_cstring_t(&query_result_data, key, value);
    query->callback(NULL, &query_result_data, query->userdata);
    amxc_var_clean(&query_result_data);
    handle_events();
}

bool __wrap_netmodel_hasFlag(const char* const interface,
                             const char* const flag,
                             const char* const condition,
                             const char* const traverse) {
    check_expected(interface);
    check_expected(flag);
    check_expected(condition);
    check_expected(traverse);
    return mock();
}

void mock_netmodel_expect_hasFlag(const char* const interface,
                                  const char* const flag,
                                  const char* const condition,
                                  const char* const traverse,
                                  bool returnvalue) {
    expect_string(__wrap_netmodel_hasFlag, interface, interface);
    expect_string(__wrap_netmodel_hasFlag, flag, flag);
    if(condition != NULL) {
        expect_string(__wrap_netmodel_hasFlag, condition, condition);
    } else {
        expect_value(__wrap_netmodel_hasFlag, condition, condition);
    }
    expect_string(__wrap_netmodel_hasFlag, traverse, traverse);
    will_return(__wrap_netmodel_hasFlag, returnvalue);
}
