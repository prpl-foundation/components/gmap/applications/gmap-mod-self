/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock_gmap.h"
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "test-setup.h"
#include "../common/dummy.h"
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include "gmap_self_disco.h"

/**
 * Parameters set with @ref gmap_device_set.
 *
 * Type is @ref AMXC_VAR_ID_HTABLE.
 * This maps the device name to the another @ref AMXC_VAR_ID_HTABLE
 * which maps the parameter naem to the parameter value.
 *
 * Unfortunately has to be global (so unfortunately tests can influence each other)
 * because gmap's functions (and therefore also their __wrap_ counterparts) do not have a
 * "this"-parameter.
 */
static amxc_var_t* s_devices_parameters_set = NULL;

void __wrap_gmap_client_init(amxb_bus_ctx_t* bus_ctx) {
    check_expected(bus_ctx);
}

amxb_bus_ctx_t* __wrap_gmap_get_bus_ctx(void) {
    return test_setup_bus_ctx();
}

bool __wrap_gmap_devices_createDevice(const char* key,
                                      const char* discovery_source,
                                      const char* tags,
                                      bool persistent,
                                      const char* default_name) {
    bool rv = mock();
    check_expected(key);
    check_expected(discovery_source);
    check_expected(persistent);
    check_expected(default_name);
    check_expected(tags);

    // If success, actually create it in the datamodel because amxs expects it.
    if(rv) {
        amxd_object_t* template_obj = amxd_dm_findf(test_setup_dm(), "Devices.Device.");
        amxd_object_t* obj = NULL;
        assert_non_null(template_obj);

        amxd_object_new_instance(&obj, template_obj, key, 0, NULL);
    }

    return rv;
}

void mock_gmap_expect_createDevice(const char* expect_key,
                                   const char* expect_discovery_source,
                                   const char* expect_tags,
                                   bool expect_persistent,
                                   const char* expect_default_name,
                                   bool return_value) {

    expect_string(__wrap_gmap_devices_createDevice, key, expect_key);
    expect_string(__wrap_gmap_devices_createDevice, discovery_source, expect_discovery_source);
    expect_string(__wrap_gmap_devices_createDevice, tags, expect_tags);
    expect_value(__wrap_gmap_devices_createDevice, persistent, expect_persistent);
    if(expect_default_name == NULL) {
        expect_value(__wrap_gmap_devices_createDevice, default_name, expect_default_name);
    } else {
        expect_string(__wrap_gmap_devices_createDevice, default_name, expect_default_name);
    }

    will_return(__wrap_gmap_devices_createDevice, return_value);
}

amxc_var_t* __wrap_gmap_device_get(UNUSED const char* key, UNUSED uint32_t flags) {

    return NULL;
}

bool __wrap_gmap_devices_linkAdd(const char* upper,
                                 const char* lower,
                                 const char* type,
                                 const char* datasource,
                                 UNUSED uint32_t priority) {

    bool rv = mock();
    check_expected(upper);
    check_expected(lower);
    check_expected(type);
    check_expected(datasource);
    return rv;
}

void mock_gmap_expect_linkAdd(const char* expect_upper, const char* expect_lower, const char* expect_type, const char* expect_datasource, bool return_value) {
    expect_string(__wrap_gmap_devices_linkAdd, upper, expect_upper);
    expect_string(__wrap_gmap_devices_linkAdd, lower, expect_lower);
    if(expect_type == NULL) {
        expect_value(__wrap_gmap_devices_linkAdd, type, expect_type);
    } else {
        expect_string(__wrap_gmap_devices_linkAdd, type, expect_type);
    }
    expect_string(__wrap_gmap_devices_linkAdd, datasource, expect_datasource);

    will_return(__wrap_gmap_devices_linkAdd, return_value);
}

bool __wrap_gmap_devices_linkReplace(const char* upper,
                                     const char* lower,
                                     const char* type,
                                     const char* datasource,
                                     UNUSED uint32_t priority) {

    bool rv = mock();
    check_expected(upper);
    check_expected(lower);
    check_expected(type);
    check_expected(datasource);
    return rv;
}

void mock_gmap_expect_linkReplace(const char* expect_upper, const char* expect_lower, const char* expect_type, const char* expect_datasource, bool return_value) {
    expect_string(__wrap_gmap_devices_linkReplace, upper, expect_upper);
    expect_string(__wrap_gmap_devices_linkReplace, lower, expect_lower);
    if(expect_type == NULL) {
        expect_value(__wrap_gmap_devices_linkReplace, type, expect_type);
    } else {
        expect_string(__wrap_gmap_devices_linkReplace, type, expect_type);
    }
    expect_string(__wrap_gmap_devices_linkReplace, datasource, expect_datasource);

    will_return(__wrap_gmap_devices_linkReplace, return_value);
}


bool __wrap_gmap_devices_linkRemove(const char* upper, const char* lower, const char* datasource) {
    bool rv = mock();
    check_expected(upper);
    check_expected(lower);
    check_expected(datasource);
    return rv;
}

void mock_gmap_expect_linkRemove(const char* expect_upper, const char* expect_lower, const char* expect_datasource, bool return_value) {
    expect_string(__wrap_gmap_devices_linkRemove, upper, expect_upper);
    expect_string(__wrap_gmap_devices_linkRemove, lower, expect_lower);
    expect_string(__wrap_gmap_devices_linkRemove, datasource, expect_datasource);
    will_return(__wrap_gmap_devices_linkRemove, return_value);
}


static void s_merge(amxc_var_t* target, const amxc_var_t* source) {
    assert_int_equal(AMXC_VAR_ID_HTABLE, amxc_var_type_of(target));
    assert_int_equal(AMXC_VAR_ID_HTABLE, amxc_var_type_of(source));

    const amxc_htable_t* source_htable = amxc_var_constcast(amxc_htable_t, source);
    amxc_htable_for_each(it, source_htable) {
        const char* key = amxc_htable_it_get_key(it);
        amxc_var_t* source_value = amxc_htable_it_get_data(it, amxc_var_t, hit);
        amxc_var_t* target_value = amxc_var_get_key(target, key, AMXC_VAR_FLAG_DEFAULT);
        if(target_value == NULL) {
            target_value = amxc_var_add_new_key(target, key);
        }
        amxc_var_copy(target_value, source_value);
    }
}

/**
 * Wrap/mock/fake/stub for @ref gmap_device_set().
 *
 * We don't want to enforce the order in which the parameters are set,
 * so we cannot use cmocka's `expect_` system.
 * So we record the parameters set and the test can check them with functions like
 * @ref mock_gmap_assert_device_param_uint32().
 */
bool __wrap_gmap_device_set(const char* device_name,
                            amxc_var_t* values) {

    amxc_var_t* parameters = NULL;

    assert_non_null(device_name);
    assert_true(device_name[0] != '\0');
    assert_non_null(values);
    assert_int_equal(AMXC_VAR_ID_HTABLE, amxc_var_type_of(values));

    // Create devices->parameters map if it does not exist
    if(s_devices_parameters_set == NULL) {
        amxc_var_new(&s_devices_parameters_set);
        amxc_var_set_type(s_devices_parameters_set, AMXC_VAR_ID_HTABLE);
    }

    // Create parameter_name->parameter_value map if it does not exist
    parameters = GET_ARG(s_devices_parameters_set, device_name);
    if(parameters == NULL) {
        amxc_htable_t parameters_htable;
        amxc_htable_init(&parameters_htable, 0);
        parameters = amxc_var_add_new_key_amxc_htable_t(s_devices_parameters_set, device_name, &parameters_htable);
        amxc_htable_clean(&parameters_htable, NULL);
    }

    // Add new parameters to known parameters
    s_merge(parameters, values);

    return true;
}

static bool s_can_convert(const amxc_var_t* value, uint32_t type_id) {
    amxc_var_t value_casted;
    int rv = 0;
    amxc_var_init(&value_casted);

    rv = amxc_var_convert(&value_casted, value, type_id);
    amxc_var_clean(&value_casted);
    return rv == 0;
}

static const amxc_var_t* s_get_param(const char* device_name, const char* parameter_name) {
    amxc_var_t* parameters = NULL;
    const amxc_var_t* actual_value;
    if(s_devices_parameters_set == NULL) {
        fail_msg("gmap_device_set was never called!");
    }
    parameters = GET_ARG(s_devices_parameters_set, device_name);
    if(parameters == NULL) {
        fail_msg("gmap_device_set was never called for device %s!", device_name);
    }

    actual_value = GET_ARG(parameters, parameter_name);
    if(actual_value == NULL) {
        fail_msg("gmap_device_set was never called for device %s with parameter %s", device_name, parameter_name);
    }
    return actual_value;
}

/**
 * Assert that @ref gmap_device_set was called for device `device_name` given parameter and value.
 *
 * The type is not strict (so if convertible to uint32 it passes).
 *
 * If not, fails the test.
 */
void mock_gmap_assert_device_param_uint32(const char* device_name, const char* parameter_name, uint32_t expected_value) {
    const amxc_var_t* actual_value = s_get_param(device_name, parameter_name);
    assert_true(s_can_convert(actual_value, AMXC_VAR_ID_UINT32));
    assert_int_equal(expected_value, amxc_var_dyncast(uint32_t, actual_value));
}

/**
 * @see @ref mock_gmap_assert_device_param_uint32
 */
void mock_gmap_assert_device_param_string(const char* device_name, const char* parameter_name, const char* expected_value) {
    const amxc_var_t* actual_value = s_get_param(device_name, parameter_name);
    assert_true(s_can_convert(actual_value, AMXC_VAR_ID_CSTRING));
    assert_string_equal(expected_value, amxc_var_constcast(cstring_t, actual_value));
}

/**
 * @see @ref mock_gmap_assert_device_param_uint32
 */
void mock_gmap_assert_device_param_bool(const char* device_name, const char* parameter_name, bool expected_value) {
    const amxc_var_t* actual_value = s_get_param(device_name, parameter_name);
    assert_true(s_can_convert(actual_value, AMXC_VAR_ID_BOOL));
    assert_true(expected_value == amxc_var_constcast(bool, actual_value));
}

void mock_gmap_clear() {
    amxc_var_delete(&s_devices_parameters_set);
}


bool __wrap_gmap_device_setActive(const char* key, bool value) {
    bool rv = mock();
    check_expected(key);
    check_expected(value);
    return rv;
}

void mock_gmap_expect_setActive(const char* expect_key, bool expect_value, bool return_value) {

    expect_string(__wrap_gmap_device_setActive, key, expect_key);
    expect_value(__wrap_gmap_device_setActive, value, expect_value);

    will_return(__wrap_gmap_device_setActive, return_value);
}

amxd_status_t __wrap_gmap_ip_device_add_address(const char* key, uint32_t family, const char* address,
                                                const char* scope, const char* status_value, const char* address_source, bool reserved) {
    check_expected(key);
    check_expected(family);
    check_expected(address);
    check_expected(scope);
    check_expected(status_value);
    check_expected(address_source);
    check_expected(reserved);
    return mock();
}

void mock_gmap_expect_ip_device_add_address(const char* key, uint32_t family, const char* address,
                                            const char* scope, const char* status_value, const char* address_source, bool reserved,
                                            amxd_status_t return_value) {

    expect_string(__wrap_gmap_ip_device_add_address, key, key);
    expect_value(__wrap_gmap_ip_device_add_address, family, family);
    expect_string(__wrap_gmap_ip_device_add_address, address, address);
    expect_string(__wrap_gmap_ip_device_add_address, scope, scope);
    if(status_value == NULL) {
        expect_value(__wrap_gmap_ip_device_add_address, status_value, status_value);
    } else {
        expect_string(__wrap_gmap_ip_device_add_address, status_value, status_value);
    }
    expect_string(__wrap_gmap_ip_device_add_address, address_source, address_source);
    expect_value(__wrap_gmap_ip_device_add_address, reserved, reserved);

    will_return(__wrap_gmap_ip_device_add_address, return_value);
}

amxd_status_t __wrap_gmap_ip_device_set_address(const char* key, uint32_t family,
                                                const char* address, const char* scope, const char* status_value, const char* address_source,
                                                bool reserved) {
    check_expected(key);
    check_expected(family);
    check_expected(address);
    check_expected(scope);
    check_expected(status_value);
    check_expected(address_source);
    check_expected(reserved);
    return mock();
}

void mock_gmap_expect_ip_device_set_address(const char* key, uint32_t family, const char* address,
                                            const char* scope, const char* status_value, const char* address_source, bool reserved,
                                            amxd_status_t return_value) {

    expect_string(__wrap_gmap_ip_device_set_address, key, key);
    expect_value(__wrap_gmap_ip_device_set_address, family, family);
    expect_string(__wrap_gmap_ip_device_set_address, address, address);
    expect_string(__wrap_gmap_ip_device_set_address, scope, scope);
    if(status_value == NULL) {
        expect_value(__wrap_gmap_ip_device_set_address, status_value, status_value);
    } else {
        expect_string(__wrap_gmap_ip_device_set_address, status_value, status_value);
    }
    expect_string(__wrap_gmap_ip_device_set_address, address_source, address_source);
    expect_value(__wrap_gmap_ip_device_set_address, reserved, reserved);

    will_return(__wrap_gmap_ip_device_set_address, return_value);
}


amxd_status_t __wrap_gmap_ip_device_get_address(const char* key, uint32_t family,
                                                const char* address, amxc_var_t* const ret_object) {
    check_expected(key);
    check_expected(family);
    check_expected(address);
    amxc_var_t* ret = (amxc_var_t*) mock();
    if(ret != NULL) {
        amxc_var_copy(ret_object, ret);
        amxc_var_delete(&ret);
        return amxd_status_ok;
    } else {
        return amxd_status_object_not_found;
    }
}

void mock_gmap_expect_ip_device_get_address(const char* key, uint32_t family,
                                            const char* address, amxc_var_t* const ret_object) {
    expect_string(__wrap_gmap_ip_device_get_address, key, key);
    expect_value(__wrap_gmap_ip_device_get_address, family, family);
    expect_string(__wrap_gmap_ip_device_get_address, address, address);
    will_return(__wrap_gmap_ip_device_get_address, ret_object);
}

amxd_status_t __wrap_gmap_ip_device_get_addresses(const char* key, amxc_var_t* target_addresses) {
    check_expected(key);
    const char* addresses_json_file = (const char*) mock();
    if(addresses_json_file != NULL) {
        amxc_var_t* ret = dummy_read_json_from_file(addresses_json_file);
        assert_non_null(ret);
        amxc_var_copy(target_addresses, ret);
        amxc_var_delete(&ret);
        return amxd_status_ok;
    } else {
        return amxd_status_object_not_found;
    }
}

void mock_gmap_ip_device_get_addresses(const char* key, const char* gmap_addresses_json_file) {
    expect_string(__wrap_gmap_ip_device_get_addresses, key, key);
    will_return(__wrap_gmap_ip_device_get_addresses, gmap_addresses_json_file);
}

amxd_status_t __wrap_gmap_ip_device_delete_address(const char* key, uint32_t family, const char* address) {
    check_expected(key);
    check_expected(address);
    check_expected(family);
    return amxd_status_ok;
}

void mock_gmap_ip_device_delete_address(const char* key, uint32_t family, const char* address) {
    expect_string(__wrap_gmap_ip_device_delete_address, key, key);
    expect_value(__wrap_gmap_ip_device_delete_address, family, family);
    expect_string(__wrap_gmap_ip_device_delete_address, address, address);
}

amxc_var_t* __wrap_gmap_config_get(const char* module, const char* option) {
    assert_string_equal(module, GMAP_CONFIG_BRIDGES_MODULE_NAME);
    assert_string_equal(option, GMAP_CONFIG_BRIDGES);
    amxc_var_t* list_of_csv = NULL;
    amxc_var_new(&list_of_csv);
    // Confusingly, gmap_config_get does not return the value,
    // but a list with first element the value, so do that here too:
    amxc_var_set_type(list_of_csv, AMXC_VAR_ID_LIST);
    amxc_var_add(cstring_t, list_of_csv, "bridge1,bridge2,bridge3,my_bridge");
    return list_of_csv;
}